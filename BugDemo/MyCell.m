//
//  MyCell.m
//  BugDemo
//
//  Created by Daniel Phillips on 05/05/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import "MyCell.h"

@interface MyCell()

@property (strong, readwrite, nonatomic) UILabel *indexNumberLabel;

@end

@implementation MyCell


- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        // Initialization code
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        [self setTranslatesAutoresizingMaskIntoConstraints:NO];

        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor orangeColor];
        [self.contentView addSubview:label];
        self.indexNumberLabel = label;

//        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        NSDictionary *views = NSDictionaryOfVariableBindings(label);
        NSMutableArray *constraintsArray = [NSMutableArray array];
        [constraintsArray addObjectsFromArray: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[label]-5-|"
                                                                                       options:0
                                                                                       metrics:nil
                                                                                         views:views] ];
        [constraintsArray addObjectsFromArray: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[label]-5-|"
                                                                                       options:0
                                                                                       metrics:nil
                                                                                         views:views] ];
        [self.contentView addConstraints:constraintsArray];
        
    }
    return self;
}

@end
