//
//  MasterViewController.m
//  BugDemo
//
//  Created by Daniel Phillips on 05/05/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import "MasterCollectionViewController.h"
#import "MyCell.h"

@interface MasterCollectionViewController ()<UIDataSourceModelAssociation>

@end

@implementation MasterCollectionViewController

#pragma mark - CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 50;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MyCell *cell = (MyCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MyCollectionCellIdentifier" forIndexPath:indexPath];

    cell.indexNumberLabel.text = [NSString stringWithFormat:@"%d", indexPath.row];
    
    return cell;
}

#pragma state preservation/restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"<MasterCollectionViewController> encodeRestorableStateWithCoder:");
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"<MasterCollectionViewController> decodeRestorableStateWithCoder:");
    [super decodeRestorableStateWithCoder:coder];
}

#pragma mark - UIDataSourceModelAssociation protocol

- (NSString *)modelIdentifierForElementAtIndexPath:(NSIndexPath *)indexPath inView:(UIView *)view
{
    NSLog(@"<MasterCollectionViewController> modelIdentifierForElementAtIndexPath:inView:");
    if( indexPath && view ){
        return [NSString stringWithFormat:@"%d", indexPath.row];
    }
    return nil;
}

- (NSIndexPath *)indexPathForElementWithModelIdentifier:(NSString *)identifier inView:(UIView *)view
{
    NSLog(@"<MasterCollectionViewController> indexPathForElementWithModelIdentifier:inView:");
    if( identifier && view ){
        
        NSNumber *targetNumber = @([identifier integerValue]);
        
        if( targetNumber ){
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:targetNumber.integerValue inSection:0];
            return indexPath;
        }
    }
    return nil;
}

- (IBAction)resizePressed:(id)sender {
    UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    flow.itemSize = CGSizeMake(120.0, 120.0);
    self.collectionView.collectionViewLayout = flow;
}

@end
