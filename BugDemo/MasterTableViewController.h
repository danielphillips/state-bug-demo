//
//  MasterTableViewController.h
//  BugDemo
//
//  Created by Daniel Phillips on 05/05/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterTableViewController : UITableViewController

@end
