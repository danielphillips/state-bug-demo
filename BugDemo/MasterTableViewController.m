//
//  MasterTableViewController.m
//  BugDemo
//
//  Created by Daniel Phillips on 05/05/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import "MasterTableViewController.h"

@interface MasterTableViewController ()<UIDataSourceModelAssociation>

@end

@implementation MasterTableViewController

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row];

    return cell;
}

#pragma state preservation/restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"<MasterTableViewController> encodeRestorableStateWithCoder:");
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"<MasterTableViewController> decodeRestorableStateWithCoder:");
    [super decodeRestorableStateWithCoder:coder];
}

#pragma mark - UIDataSourceModelAssociation protocol

- (NSString *)modelIdentifierForElementAtIndexPath:(NSIndexPath *)indexPath inView:(UIView *)view
{
    NSLog(@"<MasterTableViewController> modelIdentifierForElementAtIndexPath:inView:");
    if( indexPath && view ){
        return [NSString stringWithFormat:@"%d", indexPath.row];
    }
    return nil;
}

- (NSIndexPath *)indexPathForElementWithModelIdentifier:(NSString *)identifier inView:(UIView *)view
{
    NSLog(@"<MasterTableViewController> indexPathForElementWithModelIdentifier:inView:");
    if( identifier && view ){
        
        NSNumber *targetNumber = @([identifier integerValue]);
        
        if( targetNumber ){
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:targetNumber.integerValue inSection:0];
            return indexPath;
        }
    }
    return nil;
}

@end